var time = 0;
var running = 0;
var riel;
var time = new Date().getHours();

function start(){
	if(running == 0){
		running = 1;
		timer();
		document.getElementById("start").innerHTML = "Pause";
		document.getElementById("start").setAttribute('class' );
	}else{
		running = 0;
		document.getElementById("start").innerHTML = "Resume";
		document.getElementById("start").setAttribute('class' );
	}
}

function reset(){
	running = 0;
	time = 0;
	document.getElementById("start").innerHTML = "Start";
	document.getElementById("result").innerHTML = "00:00:00";
	document.getElementById("start").setAttribute('class');
}

function timer(){
	if(running == 1){
		setTimeout(function(){
			time++;
			var mins = Math.floor(time/10/60)%60;
			var secs = Math.floor(time/10)%60;
			var ms = time%10;
			
			if(mins<10){
				mins = "0" + mins;
			}
			if(secs<10){
				secs = "0" + secs;
			}
			
			document.getElementById("result").innerHTML = mins+ ":" +secs+ ":" + "0" +ms;
			timer(); 
		},100);
	}
}

function Calculate() {
	if(running == 0){
		running = 1;
  var riel;
  var time = new Date().getHours();
  if (time < 15) {
    riel = time*500;
  } else if (time >=16 && time<=30) {
    riel = time*1000;
  } else if (time >=31 && time<=60) {
    riel = time*1500;
  }else {
    riel = time*2000;
  }
  document.getElementById("count").innerHTML = riel;
}
}